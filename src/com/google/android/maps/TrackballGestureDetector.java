/*
 * Copyright (C) 2009 James Ancona
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.android.maps;

import android.view.MotionEvent;

/**
 * @author Jim Ancona
 */
public class TrackballGestureDetector {

    public void analyze(MotionEvent motionevent) {
        throw new NotImplementedException();
    }

    public void registerLongPressCallback(Runnable runnable) {
        throw new NotImplementedException();
    }

    public boolean isScroll() {
        throw new NotImplementedException();
    }

    public float scrollX() {
        throw new NotImplementedException();
    }

    public float scrollY() {
        throw new NotImplementedException();
    }

    public boolean isTap() {
        throw new NotImplementedException();
    }

    public float getCurrentDownX() {
        throw new NotImplementedException();
    }

    public float getCurrentDownY() {
        throw new NotImplementedException();
    }

    public boolean isDoubleTap() {
        throw new NotImplementedException();
    }

    public float getFirstDownX() {
        throw new NotImplementedException();
    }

    public float getFirstDownY() {
        throw new NotImplementedException();
    }
}
