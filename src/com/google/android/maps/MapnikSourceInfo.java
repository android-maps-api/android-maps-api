/*
 * Copyright (C) 2009 James Ancona
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.android.maps;

/**
 * {@hide}
 * @author Jim Ancona
 */
class MapnikSourceInfo implements MapSourceInfo {
    public int getMaxZoom() {
        return 18;
    }

    public String getName() {
        return "Open Street Maps Mapnik renderer";
    }

    public String getTileUri(int x, int y, int zoom) {
        return "http://tile.openstreetmap.org/" + zoom + "/" + x + "/" + y + ".png";
    }

    public int getTileSize() {
        return 256;
    }

    public String getAttribution() {
        return "Map data licensed CC-BY-SA OpenStreetMap\n(OpenStreetMap Foundation and contributors)";
    }
    

}
